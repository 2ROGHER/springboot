package com.freecamp.finalproject.models;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "sellers")
public class Seller {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id; // RUC
    private String name;
    private String lastName;
    private String email;
    private String phone;
    private String direction;
    private String picture;

    @OneToMany(mappedBy = "seller", cascade = CascadeType.ALL)
    private ArrayList<Product> products;

    public Seller() {
    }

    public Seller(UUID id,
                  String name,
                  String lastName,
                  String email,
                  String phone,
                  String direction,
                  String picture,
                  ArrayList<Product> products) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.direction = direction;
        this.picture = picture;
        this.products = products;
    }

    // Methods

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seller seller = (Seller) o;
        return Objects.equals(getId(), seller.getId()) && Objects.equals(getName(), seller.getName()) && Objects.equals(getLastName(), seller.getLastName()) && Objects.equals(getEmail(), seller.getEmail()) && Objects.equals(getPhone(), seller.getPhone()) && Objects.equals(getDirection(), seller.getDirection()) && Objects.equals(getPicture(), seller.getPicture()) && Objects.equals(getProducts(), seller.getProducts());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getLastName(), getEmail(), getPhone(), getDirection(), getPicture(), getProducts());
    }

    @Override
    public String toString() {
        return "Seller{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", direction='" + direction + '\'' +
                ", picture='" + picture + '\'' +
                ", products=" + products +
                '}';
    }
}
