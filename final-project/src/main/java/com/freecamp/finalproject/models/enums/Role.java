package com.freecamp.finalproject.models.enums;

import org.springframework.security.core.GrantedAuthority;

/**
 * This model is used to represent the Role for each Costumer.
 * This can be (USER, ADMIN, SUPERUSER, ROOT, CUSTOMER, etc).
 */
public enum Role implements GrantedAuthority {
    ROLE_USER("USER"),
    ROLE_ADMIN("ADMIN");

    private String value;

    Role(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String getAuthority() {
        return null;
    }


}
