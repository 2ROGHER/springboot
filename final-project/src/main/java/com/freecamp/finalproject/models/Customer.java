package com.freecamp.finalproject.models;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name="customers")
public class Customer {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private UUID id;
    private String name;
    private String lastName;
    private String email;
    private Long dni;
    private String phone;
    private String direction;
    private List<String> pictures;

    // One-to-one relationship between User and Client entity
    @OneToOne(cascade = CascadeType.ALL) // delete cascade type
    @JoinColumn(name="user_id", referencedColumnName = "id")
    @JsonManagedReference
    private User user;


    // Basic relationship ManyToMany
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name="customer_products", // table name
            joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"), // id relationship for product
            inverseJoinColumns = @JoinColumn(name = "customer_id", referencedColumnName = "id") // id relationship for user
    )
    private List<Product> products;

    public Customer() {}


    public Customer(UUID id,
                    String name,
                    String lastName,
                    String email,
                    Long dni,
                    String phone,
                    String direction,
                    List<String> pictures,
                    User user,
                    List<Product> products) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.dni = dni;
        this.phone = phone;
        this.direction = direction;
        this.pictures = pictures;
        this.user = user;
        this.products = products;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getDni() {
        return dni;
    }

    public void setDni(Long dni) {
        this.dni = dni;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        if(this.products == null) {
            this.products = new ArrayList<>();
        }
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(getId(),
                customer.getId()) && Objects.equals(getName(), customer.getName()) && Objects.equals(getLastName(), customer.getLastName()) && Objects.equals(getEmail(), customer.getEmail()) && Objects.equals(getDni(), customer.getDni()) && Objects.equals(getPhone(), customer.getPhone()) && Objects.equals(getDirection(), customer.getDirection()) && Objects.equals(getPictures(), customer.getPictures()) && Objects.equals(getUser(), customer.getUser()) && Objects.equals(getProducts(), customer.getProducts());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getLastName(), getEmail(), getDni(), getPhone(), getDirection(), getPictures(), getUser(), getProducts());
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", dni=" + dni +
                ", phone='" + phone + '\'' +
                ", direction='" + direction + '\'' +
                ", picture='" + pictures + '\'' +
                ", user=" + user +
                ", products=" + products +
                '}';
    }
}
