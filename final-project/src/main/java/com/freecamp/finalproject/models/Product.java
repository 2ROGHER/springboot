package com.freecamp.finalproject.models;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String title;
    private String description;
    private int quantity;
    private boolean inStock;
    @ElementCollection
    private List<String> pictures;
    private double price;
    private int stars;

    // Many-to-many relationships
    @ManyToMany(mappedBy = "products", cascade = CascadeType.ALL) // mapped by users entity or model.
    private List<Customer> customers;

    // One-to-many relationship with Seller entity
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "seller_id", nullable = false)
    private Seller seller;

    // Many-to-one relationship with Selle entity
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    // Constructor

    public Product() {
    }

    public Product(UUID id,
                   String title,
                   String description,
                   int quantity,
                   boolean inStock,
                   List<String> pictures,
                   double price,
                   int stars,
                   List<Customer> customers,
                   Seller seller,
                   Category category) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.quantity = quantity;
        this.inStock = inStock;
        this.pictures = pictures;
        this.price = price;
        this.stars = stars;
        this.customers = customers;
        this.seller = seller;
        this.category = category;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    // We are implementing a constructor without initializing for relationship fields.


    // Methods

}
