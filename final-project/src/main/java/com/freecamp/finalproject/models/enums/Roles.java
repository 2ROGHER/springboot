package com.freecamp.finalproject.models.enums;

public enum Roles {
    ADMIN,
    SUPER,
    GUEST,
    EDITOR,
    MANAGER,
    DEVELOPER,
    AUDITOR,
    CUSTOMER, // by default

}
