package com.freecamp.finalproject.utils.logger.interfaces;

public interface ILogMessage {
    public void message(String message);
}
