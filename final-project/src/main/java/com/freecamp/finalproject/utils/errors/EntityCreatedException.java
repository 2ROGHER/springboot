package com.freecamp.finalproject.utils.errors;

public class EntityCreatedException extends RuntimeException  {
    public EntityCreatedException(String message) {
        super(message);
    }
}
