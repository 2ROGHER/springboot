package com.freecamp.finalproject.utils.errors;

public class IDEmptyNullException extends RuntimeException {
    public IDEmptyNullException(String message) {
        super(message);
    }
}
