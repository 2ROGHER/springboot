package com.freecamp.finalproject.utils.errors;

public class EntityNotUpdatedException extends RuntimeException {
    public EntityNotUpdatedException(String message) {
        super(message);
    }
}
