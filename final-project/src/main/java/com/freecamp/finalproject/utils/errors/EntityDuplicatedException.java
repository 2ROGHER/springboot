package com.freecamp.finalproject.utils.errors;

public class EntityDuplicatedException extends RuntimeException {
    public EntityDuplicatedException(String message) {
        super(message);
    }
}
