package com.freecamp.finalproject.utils.logger;

import com.freecamp.finalproject.utils.logger.interfaces.ILogMessage;


import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.ansi;

public class LogError implements ILogMessage {
    @Override
    public void  message(String message) {
        System.out.println(ansi().eraseScreen().fg(RED).a("+[ERROR] " + message));
    }
}
