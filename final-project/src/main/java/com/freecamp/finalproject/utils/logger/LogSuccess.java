package com.freecamp.finalproject.utils.logger;

import com.freecamp.finalproject.utils.logger.interfaces.ILogMessage;


import static org.fusesource.jansi.Ansi.Color.BLUE;
import static org.fusesource.jansi.Ansi.ansi;

public class LogSuccess implements ILogMessage {

    @Override
    public void message(String message) {
        System.out.println(ansi().eraseScreen().fg(BLUE).a("+[SUCCESS] " + message));
    }
}

