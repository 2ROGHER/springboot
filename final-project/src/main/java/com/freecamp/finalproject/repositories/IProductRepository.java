package com.freecamp.finalproject.repositories;

import com.freecamp.finalproject.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface IProductRepository extends JpaRepository<Product, UUID> {}