package com.freecamp.finalproject.repositories;
import com.freecamp.finalproject.models.User;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IUserRepository extends JpaRepository<User, UUID> {
    public User findByUsername(String username);
    public List<User> findAllByActive(boolean active);
}
