package com.freecamp.finalproject.repositories;

import com.freecamp.finalproject.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ICustomerRepository extends JpaRepository<Customer, UUID> {
    List<Customer>  getCustomerByName(String name);
}
