package com.freecamp.finalproject.repositories;

import com.freecamp.finalproject.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ICategoryRepository extends JpaRepository<Category, UUID> {
}
