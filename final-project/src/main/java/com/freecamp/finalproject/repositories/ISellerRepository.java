package com.freecamp.finalproject.repositories;

import com.freecamp.finalproject.models.Seller;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ISellerRepository extends JpaRepository<Seller, UUID> {
}
