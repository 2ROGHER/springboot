package com.freecamp.finalproject.services;

import com.freecamp.finalproject.dto.AuthRequestDTO;
import com.freecamp.finalproject.dto.AuthResponseDTO;
import com.freecamp.finalproject.models.Customer;
import com.freecamp.finalproject.models.User;
import com.freecamp.finalproject.repositories.ICustomerRepository;
import com.freecamp.finalproject.repositories.IUserRepository;
import com.freecamp.finalproject.security.service.JwtService;
import com.freecamp.finalproject.utils.errors.EntityDuplicatedException;
import com.freecamp.finalproject.utils.errors.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@Service
public class AuthService {
    @Autowired
    private ICustomerRepository _customerRepository;

    @Autowired
    private IUserRepository _userRepository;

    @Autowired
    private AuthenticationManager _authenticationManager;

    // AuthenticationManager from SecurityConfig.class
    @Autowired
    private JwtService _jwtService;

    @Autowired
    private PasswordEncoder _passwordEncoder;

    public AuthResponseDTO login (AuthRequestDTO auth) throws  Exception {

        try {
            Authentication authentication = _authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            auth.getUsername(), auth.getPassword()
                    )
            );

            User user = (User) authentication.getPrincipal();
            String token = _jwtService.generateToken(user);

            if(_passwordEncoder.matches(auth.getPassword(), user.getPassword())) {
                System.out.println("Password and username matches");
                return new AuthResponseDTO(token);
            } else {
                throw new Exception("Password and username does not match");
            }
        } catch (BadCredentialsException e) {
            throw  new BadCredentialsException("Invalid credentials");
        } catch (Exception e) {
            throw new Exception("User is blocked or inactive");
        }
    }

    public Customer register(Customer customer) {
        try {
            // Find customer by username
            List<Customer> customers = this._customerRepository.getCustomerByName(customer.getName());

            if(customers.isEmpty()) {
               if(customer.getUser() != null) {
                   customer.getUser().setCustomer(customer);
               };

               // encrypt the password and set active
                customer.getUser().setPassword(this._passwordEncoder.encode(customer.getUser().getPassword()));
                customer.getUser().setActive(true);
                customer.getUser().setAccountNonExpired(true);
                customer.getUser().setAccountNonLocked(true);
                customer.getUser().setEnabled(true);
                customer.getUser().setCredentialsNonExpired(true);

                System.out.println("User account non expired: " + customer.getUser().isAccountNonExpired());

               return this._customerRepository.save(customer);
            } else {
                throw new EntityDuplicatedException("Customer with name " + customer.getName() + " already exists");
            }

        } catch (Exception e) {
            throw new EntityNotFoundException("Customer with name " + customer.getName()+ " not found in database" + e.getMessage());
        }
    }

}
