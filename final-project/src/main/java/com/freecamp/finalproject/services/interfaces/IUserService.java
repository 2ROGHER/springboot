package com.freecamp.finalproject.services.interfaces;

import com.freecamp.finalproject.models.User;

import java.util.List;
import java.util.UUID;

public interface IUserService {
    // Define all methods to implements in other classes | controllers | rest
    List<User> getAllUsers();
    User getUserById(UUID id);
    User deleteUserById(UUID id);
    User udpateUserById(UUID id);
}
