package com.freecamp.finalproject.services;

import com.freecamp.finalproject.dto.UserDTO;
import com.freecamp.finalproject.models.User;
import com.freecamp.finalproject.repositories.IUserRepository;
import com.freecamp.finalproject.utils.errors.EntityNotUpdatedException;
import com.freecamp.finalproject.utils.errors.IDEmptyNullException;
import com.freecamp.finalproject.utils.errors.EntityNotFoundException;
import com.freecamp.finalproject.utils.logger.LogError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

// TODO: This class service needs to implement all methods defined in IAuthService class.

/**
 * This class must be implemented ClientsService interface to authenticate & authorize
 * login requests.
 */
@Service

public class UserService implements UserDetailsService {
    @Autowired
    private  IUserRepository _userRepository;

    public User createUserService(User user) {
        return this._userRepository.save(user);
    }

    public User getUserById(UUID id) throws EntityNotFoundException{
        try {
            return this._userRepository.findById(id)
                    .orElseThrow(() -> new EntityNotFoundException("Not foud entity" + id) );
        } catch (Exception e) {
            throw new EntityNotFoundException("Entity  User with id: " + id + " not found");
        }
    }

    public User getUserByName(String username) {
        return null;
    }

    public Optional<User> deleteUserService(UUID id) throws IDEmptyNullException, EntityNotFoundException {
        Optional<User> userDeleted = this._userRepository.findById(id);
        if(userDeleted.isEmpty()) {
            throw new EntityNotFoundException("User not found in database error");
        }

        this._userRepository.deleteById(id);
        return userDeleted;
    }

    public User updateUserService(User user, UUID id) throws EntityNotFoundException, EntityNotUpdatedException {
        if(id == null) {
            new LogError().message("ID " + id + "is null or empty, please check it.");
            throw  new IDEmptyNullException("ID is null or empty, required");
        }


        try {
            return this._userRepository.findById(id)
                    .map(item -> {
                        item.setUsername(user.getUsername());
                        item.setPassword(user.getPassword());
                        item.setActive(user.isActive());
                        item.setRoles(user.getRoles());
                        item.setPictures(user.getPictures());
                        return this._userRepository.save(item);
                    })
                    .orElseGet(() -> {
                        return this._userRepository.save(user);
                    });
        } catch( Exception e) {
            throw  new  EntityNotUpdatedException("Could not update user with id " + id);
        }
    }


    /**
     * This method is used to load and generate an UserDetail Object for authenticated purposes.
     * This method is used with Spring Security framework
     * @param username
     * @return UserDetail Object
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User user = this._userRepository.findByUsername(username);
            return new UserDTO(user);
        } catch (UsernameNotFoundException e) {
            throw new UsernameNotFoundException("User name " + username + "not found");
        }
    }


}
