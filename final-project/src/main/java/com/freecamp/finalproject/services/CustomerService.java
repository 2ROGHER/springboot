package com.freecamp.finalproject.services;

import com.freecamp.finalproject.models.Customer;
import com.freecamp.finalproject.models.User;
import com.freecamp.finalproject.repositories.ICustomerRepository;
import com.freecamp.finalproject.utils.errors.EntityCreatedException;
import com.freecamp.finalproject.utils.errors.EntityDuplicatedException;
import com.freecamp.finalproject.utils.errors.EntityNotFoundException;
import com.freecamp.finalproject.utils.errors.IDEmptyNullException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerService {
    @Autowired
    private  ICustomerRepository _customerRepository;


    public Customer createCustomerService(Customer client)  throws EntityCreatedException {
        return this._customerRepository.save(client);
    }

    public Optional<Customer> getCustomerByIdService(UUID id) throws EntityNotFoundException, IDEmptyNullException {
        if (id == null) throw new IDEmptyNullException("ID " + id + " is null or empty");

        Optional<Customer> customer = this._customerRepository.findById(id);

        if(customer.isPresent()) throw new EntityNotFoundException("client with id " + customer.get().getId() + "not found in database");

        return this._customerRepository.findById(id);

    }

    public Customer updateCustomerService(Customer client) throws EntityNotFoundException {
        Optional<Customer> updatedClient = this._customerRepository.findById(client.getId());
        if(!updatedClient.isPresent()) throw new EntityNotFoundException("client with id " + client.getId() + "not found in database");

        return this._customerRepository.save(client);
    }

    public Optional<?> deleteCustomerService(UUID id) throws EntityNotFoundException, IDEmptyNullException {
        if(id == null) throw new IDEmptyNullException("ID null or empty");

        Optional<Customer> customer = this._customerRepository.findById(id);

        if(customer.isEmpty()) throw  new EntityNotFoundException("client with id " + customer.get().getId() + " not found in database");

        this._customerRepository.deleteById(id);

        return customer;
    }



}
