package com.freecamp.finalproject.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/api/v1/")
    public String index () {
        System.out.println("Login successful");
        return "Hello World!";
    }

    @GetMapping("/api/v1/index")
    public String viewIndex() {
        return """
                <h1>hello world</h1>
                <button style="background: #0000f2; color: #fff;padding: 16px 32px;"><a href="https://spring.io/docs">go</a></button>
               """;
    }
}
