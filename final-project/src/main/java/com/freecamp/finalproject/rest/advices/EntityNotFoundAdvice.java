package com.freecamp.finalproject.rest.advices;

import com.freecamp.finalproject.utils.errors.EntityNotFoundException;
import com.freecamp.finalproject.utils.errors.EntityNotUpdatedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EntityNotFoundAdvice {

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String entityNotFoundHandler(EntityNotUpdatedException e) {
        return e.getMessage();
    }

}
