package com.freecamp.finalproject.rest;

import com.freecamp.finalproject.models.User;
import com.freecamp.finalproject.repositories.IUserRepository;
import com.freecamp.finalproject.services.UserService;
import com.freecamp.finalproject.services.interfaces.IUserService;
import com.freecamp.finalproject.utils.logger.LogError;
import com.freecamp.finalproject.utils.logger.LogSuccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

// TODO: Implements authentication security.
@RestController
public class UserController {
    @Autowired
    private IUserRepository _userRepository;

    @Autowired
    private UserService _userService;


    @GetMapping("/api/v1/users")
    public List<User> getAllUsers() {
        return this._userRepository.findAll();
    }

    @PostMapping("/api/v1/users")
    public ResponseEntity<?> createUser(@RequestBody User user) {
        try {
            User response = this._userService.createUserService(user);
            ResponseEntity.ok(response);

        } catch(Exception e) {
            // Handle error.
            return ResponseEntity.status(500).body("Error in" + e.getMessage());

        }
        return null;
    }

    @DeleteMapping("/api/v1/users/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable UUID id) {
        try {
            Optional<User> response = this._userService.deleteUserService(id);
            new LogSuccess().message("User deleted successfully with ID" + id);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            new LogError().message("Error while deleting user: " + e.getMessage());
            return ResponseEntity.badRequest().body("Error deleting user: " + e.getMessage());
        }
    }

    @PutMapping("/api/v1/users/update/{id}")
    public ResponseEntity<?> updateUser(@RequestBody User user, @PathVariable UUID id) {
        System.out.println("the id is : " + id);
        try {
            User response = this._userService.updateUserService(user, id);
            new LogSuccess().message("User updated successfully with ID " + id);
            return ResponseEntity.ok(response);
        } catch(Exception e) {
            new LogError().message("Error updating user: " + e.getMessage());
            return ResponseEntity.badRequest().body("Error updating user: " + e.getMessage());
        }
    }



}
