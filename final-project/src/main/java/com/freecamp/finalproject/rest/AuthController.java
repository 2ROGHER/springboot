package com.freecamp.finalproject.rest;

import com.freecamp.finalproject.dto.AuthRequestDTO;
import com.freecamp.finalproject.dto.AuthResponseDTO;
import com.freecamp.finalproject.models.Customer;
import com.freecamp.finalproject.models.User;
import com.freecamp.finalproject.security.config.SecurityConfig;
import com.freecamp.finalproject.security.service.JwtService;
import com.freecamp.finalproject.services.AuthService;
import com.freecamp.finalproject.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class AuthController {

    @Autowired
    private AuthService _authService;

    @PostMapping("/api/v1/auth/login")
    public ResponseEntity<?> login(@RequestBody AuthRequestDTO auth) throws Exception {
       try {
           return ResponseEntity.ok(this._authService.login(auth));
       } catch (Exception e) {
          return ResponseEntity.internalServerError().body("Error while login" + e.getMessage());
       }
    }

    @PostMapping("/api/v1/auth/register")
    public ResponseEntity<?> register(@RequestBody Customer customer) {
        try {
            return ResponseEntity.ok(this._authService.register(customer));
        } catch (Exception e) {
            return ResponseEntity.status(500).body("An error occurred while registering new customer" + e.getMessage());
        }
    }

    public Optional<User> logout() {
        //TODO: implement it later.
        return Optional.empty();
    }
}
