package com.freecamp.finalproject.rest;

import com.freecamp.finalproject.models.Customer;
import com.freecamp.finalproject.repositories.ICustomerRepository;
import com.freecamp.finalproject.services.CustomerService;
import jakarta.websocket.server.PathParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class CustomerController {
    private final ICustomerRepository _clientRepository;
    private final CustomerService _clientService;

    // dependency injection
    public CustomerController(ICustomerRepository clientRepository, CustomerService clientService) {
        this._clientRepository = clientRepository;
        this._clientService = clientService;
    }

    @GetMapping("/api/v1/hello")
    public String index() {
        return "hello world";
    }

    @GetMapping("/api/v1/clients")
    public List<Customer> getClient() {
       return this._clientRepository.findAll();
    }

    @PostMapping("/api/v1/clients")
    public ResponseEntity<?> createClient(@RequestBody Customer client) {
        try {
            Customer response = this._clientService.createCustomerService(client);
            return ResponseEntity.ok(response);
        } catch(Exception e) {
            return ResponseEntity.badRequest().body("Error creating new client" + e.getMessage());
        }
    }

    @GetMapping("/api/v1/client/{id}")
    public ResponseEntity<?> getClientById(@PathVariable UUID id) {
        try {
            return  ResponseEntity.ok(this._clientService.getCustomerByIdService(id));
        } catch(Exception e) {
            return ResponseEntity.badRequest().body("Error get client by ID" + e.getMessage());
        }
    }

    @PutMapping("/api/v1/clients")
    public ResponseEntity<?> updateClient(@RequestBody Customer client) {
        try {
            return ResponseEntity.ok(this._clientService.updateCustomerService(client));
        } catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error updating client: " + e.getMessage());
        }
    }

    @DeleteMapping("/api/v1/clients/{id}")
    public ResponseEntity<?> deleteClient(@PathVariable UUID id) {
        try {
            return ResponseEntity.ok(this._clientService.deleteCustomerService(id));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error deleting client: " + e.getMessage());
        }
    }

    // This route allows to add products to card or favorites
    @PutMapping("/api/v1/clients/favorites/{id}")
    public void addProductsToFavorites(@PathParam("id") UUID id, Customer client) {
        this._clientRepository.save(client);
    }

}
