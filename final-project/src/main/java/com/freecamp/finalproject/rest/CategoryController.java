package com.freecamp.finalproject.rest;

import com.freecamp.finalproject.models.Category;
import com.freecamp.finalproject.repositories.ICategoryRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {
    private ICategoryRepository _categoryRepository;

    public CategoryController(ICategoryRepository categoryRepository) {
        this._categoryRepository = categoryRepository;
    }

    @GetMapping("/api/v1/categories")
    public List<Category> getAllCategories() {
        return this._categoryRepository.findAll();
    }

    @PostMapping("/api/v1/categories")
    public Category createCategory(@RequestBody Category category) {
        return this._categoryRepository.save(category);
    }
}
