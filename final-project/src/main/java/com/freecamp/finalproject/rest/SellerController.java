package com.freecamp.finalproject.rest;

import com.freecamp.finalproject.models.Seller;
import com.freecamp.finalproject.repositories.ISellerRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class SellerController {
    private ISellerRepository _sellerRepository;

    public SellerController(ISellerRepository sellerRepository) {
        this._sellerRepository = sellerRepository;
    }

    @GetMapping("/api/v1/sellers")
    public List<Seller> getAllSellers() {
        return this._sellerRepository.findAll();
    }

    @GetMapping("/api/v1/sellers/{id}")
    public Optional<Seller> getAllSellersById(@PathVariable UUID id) {
        System.out.println("Here in the route" + id);
        return (Optional<Seller>)  this._sellerRepository.findById(id);
    }

    @PostMapping("/api/v1/sellers")
    public Seller createSeller(@RequestBody Seller seller) {
        return this._sellerRepository.save(seller);
    }

    @PutMapping("/api/v1/sellers")
    public Seller updateSeller(@RequestBody Seller seller) {
        return this._sellerRepository.save(seller);

    }

    @DeleteMapping("/api/v1/sellers")
    public void deleteSeller(@RequestBody Seller seller) {
        this._sellerRepository.delete(seller);
    }
}
