package com.freecamp.finalproject.rest;

import com.freecamp.finalproject.models.Product;
import com.freecamp.finalproject.repositories.IProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class ProductController {

    @Autowired
    private IProductRepository _productRepository;


    @GetMapping("/api/v1/products")
    public List<Product> getAllProducts() {
        return  this._productRepository.findAll();
    }

    @PostMapping("/api/v1/products")
    public Product createProduct(@RequestBody Product product) {
        return this._productRepository.save(product);
    }

    @GetMapping("/api/v1/products/{id}")
    public Optional<Product> getProductById(@PathVariable UUID id) {
        return (Optional<Product> )this._productRepository.findById(id);
    }

    @PutMapping("/api/v1/products")
    public Product udpdateProduct(@RequestBody Product product) {
        return this._productRepository.save(product);
    }

    @DeleteMapping("/api/v1/products")
    public void deleteProduct(@PathVariable UUID id) {
        System.out.println("Deleting with id" + id);
    }

//    @GetMapping("/api/v1/products")
//    public ResponseEntity<?> getProductByName(@RequestParam String name) {
//        // TODO: implement this method later.
//        return null;
//
//    }
//
//    @GetMapping("/api/v1/products")
//    public ResponseEntity<?> getProductByDescription(@RequestParam String description) {
//        // TODO: implement this method later.
//        return null;
//
//    }
//
//    // Add producto to favorites
//    @PostMapping("/api/v1/products/favorites")
//    public ResponseEntity<?> addProductToFavorites(@RequestBody Product product) {
//        // TODO: implement this method later.
//        return null;
//    }
//
//    // Add product to car
//    @PostMapping("/api/v1/products/car")
//    public ResponseEntity<?> addProductToCar(@RequestBody Product product) {
//        // TODO: implement this later
//        return null;
//    }
//
//    // Payment method
//    @PostMapping("/api/v1/products/payment")
//    public ResponseEntity<?> doPayment(@PathVariable UUID id) {
//        return null;
//    }

}
