package com.freecamp.finalproject.config;

import com.freecamp.finalproject.models.Customer;
import com.freecamp.finalproject.models.User;
import com.freecamp.finalproject.models.enums.Roles;
import com.freecamp.finalproject.repositories.IUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Configuration
public class LoadDatabase {

    private static final Logger logger = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    public CommandLineRunner loadDatabaseCommandLineRunner(IUserRepository repository) {
        List<String> pictures = Arrays.asList("url 1", "url 2");



        return args -> {
//            logger.info("Preloading data: " + repository.save(new User(null, "username 1", "password 1", true, pictures,Roles.CUSTOMER )));
//            logger.info("Preloading data: " + repository.save(new User(null, "username 2", "password 2", true, pictures,Roles.CUSTOMER )));
//            logger.info("Preloading data: " + repository.save(new User(null, "username 3", "password 3", true, pictures,Roles.CUSTOMER )));
//            logger.info("Preloading data: " + repository.save(new User(null, "username 4", "password 4", true, pictures,Roles.CUSTOMER )));

        };
    }

}
