package com.freecamp.finalproject;

import com.freecamp.finalproject.config.LoadDatabase;
import com.freecamp.finalproject.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.freecamp.finalproject")
public class FinalProjectApplication {

	@Autowired
	private LoadDatabase loadDatabase;

	public static void main(String[] args) {
		SpringApplication.run(FinalProjectApplication.class, args);
		// get the bean context and create all products


	}


}
