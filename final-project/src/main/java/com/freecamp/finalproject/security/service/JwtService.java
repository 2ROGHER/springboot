package com.freecamp.finalproject.security.service;

import com.freecamp.finalproject.models.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

// This is the master of the ceremony
// This is pivotal component that orchestratete the validation and creation of JWT tokens.
@Component
public class JwtService {
    // This is the secret key that is used to firm and validate the token in server side.
    @Value("${app.security.jwt.secret}")
    private   String secret;
    @Value("${app.security.jwt.expiration}")
    private long expiration;

//    public String extractUsername(String token) {
//        return extractClaim(token, Claims::getSubject);// "::" operador de referencia de metodos y constructores sin invocarlos.
//
//    }
//
//    public Date extractExpiration(String token) {
//        return extractClaim(token, Claims::getExpiration);
//    }
//
//    private  <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
//        final Claims claims = extractAllClaims(token);
//        return claimsResolver.apply(claims);
//    }
//
//    private Claims extractAllClaims(String token) {
//        return Jwts
//                .parserBuilder()
//                .setSigningKey(getSignKey())
//                .build()
//                .parseClaimsJws(token)
//                .getBody();
//    }
//
//    private Boolean isTokenExpired(String token) {
//        return extractExpiration(token).before(new Date());
//    }
//
//    // TODO: change UserDetails interface for UserDTO
//    public Boolean validateToken(String token, UserDetails userDetails) {
//        final String username = extractUsername(token);
//        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
//    }
//
//    public String generateToken(String username) {
//        Map<String, Object> claims = new HashMap<>();
//        return createToken(claims, username);
//    }
//
//    // This take a String map y returns an Object
//    private String createToken(Map<String, Object> claims, String username) {
//        return Jwts
//                .builder()
//                .setClaims(claims)
//                .setSubject(username)
//                .setIssuedAt(new Date(System.currentTimeMillis()))
//                .setExpiration(new Date(System.currentTimeMillis()+ 1000*60*1)) //  1 hour
//                .signWith(getSignKey(), SignatureAlgorithm.HS512).compact();
//    }
//
//    private Key getSignKey() {
//        byte[] keyBytes = Decoders.BASE64.decode(SECRET);
//        return Keys.hmacShaKeyFor(keyBytes);
//    }

    // Simple and fast method 2nd method.

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);// "::" operador de referencia de metodos y constructores sin invocarlos.

    }

    public Date extractExpirationDate(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    private  <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String generateToken(User user) {
        Map<String, Object> claims = new HashMap<>(); // [{ key: value }]
        claims.put("id", user.getId());
        claims.put("name", user.getUsername());
        claims.put("password", user.getPassword());
        claims.put("roles", user.getRoles());

        return createToken(claims, user.getUsername());
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        Date expirationDate = extractExpirationDate(token); // Extract expiration date from token
        if (expirationDate.before(new Date())) {
            return false;
        }

        String username = extractUsername(token);
        return (userDetails.getUsername().equalsIgnoreCase(username) && !expirationDate.before(new Date()));

    }

    private String createToken(Map<String, Object> claims, String username) {
        return Jwts
                .builder()
                .setClaims(claims)
                .setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expiration))
                .signWith(getSignKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    private Key getSignKey() {
        byte[] keyBytes = Decoders.BASE64.decode(secret);
        return Keys.hmacShaKeyFor(keyBytes);
    }

}
