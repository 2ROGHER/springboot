package com.freecamp.finalproject.security.jwt;


import com.freecamp.finalproject.security.service.JwtService;
import com.freecamp.finalproject.services.UserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

// This is the sentinel of the security app with Spring Boot security
@Component
public class JwtAuthFilter extends OncePerRequestFilter {

    // We need to inject the dependencies with the @Autowired annotation
    @Autowired
    private JwtService _jwtService;

    @Autowired
    private UserService _userService;



//    @Override
//    protected void doFilterInternal(HttpServletRequest request,
//                                    HttpServletResponse response,
//                                    FilterChain filterChain) throws ServletException, IOException {
//        String authHeader = request.getHeader("Authorization");
//        System.out.println("Los headers son: " + authHeader);
//        String token = null;
//        String username = null;
//
//        if(authHeader != null && authHeader.startsWith("Bearer ")) {
//            token = authHeader.substring(7); //TODO: verify this is correct or not
//            username = _jwtService.extractUsername(token);
//        }
//
//        if(username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
//            System.out.println("The username is:  " + username);
//            UserDetails userDetails = _userService.loadUserByUsername(username); // Search User by username
//            System.out.println("The user details is: " + userDetails);
//            if(_jwtService.validateToken(token, userDetails)) {
//                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getAuthorities() ); // TODO: replace userDetails.getAuthorities() for find all authorities for user.
//                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
//            }
//        }
//
//        filterChain.doFilter(request, response);
//    }

    // Other version for this JwAuthFilter

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String authHeader  = request.getHeader("Authorization"); // Get authorization header from the request
        System.out.println("Los headers son: " + authHeader);
        if(authHeader != null && authHeader.startsWith("Bearer ")) {
            String token = authHeader.substring(7); // Cut the string and get the token from the request.
            String username = _jwtService.extractUsername(token); // Extract the username from the token extracted.

            System.out.println("username: " + username);
            System.out.println("token: " + token);

            if(username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails user = _userService.loadUserByUsername(username);

                System.out.println("the user is" + user.getUsername());
                if(_jwtService.validateToken(token, user)) {
                    UsernamePasswordAuthenticationToken autToken = new UsernamePasswordAuthenticationToken(
                            user, // user founded by username
                            null,  // no implement 'credentials' yet
                            user.getAuthorities() // get authorities from UserDetails class
                    );

                    System.out.println("Authentication token" + autToken);
                }
            }
        }

        // Do the filter for every request.
        filterChain.doFilter(request, response);

    }

}
