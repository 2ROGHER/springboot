package com.openbootcamp.springboot.repositories;

import com.openbootcamp.springboot.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
* La finalidad de esta interfaz es permitir realizar operaciones CRUD con la entidad "BOOK".
* Esta es la mejor práctica para poder realizar persistence de los datos.
*
* */
@Repository // Con esta notacion le decimos que esta interfaz es un `@bean` que va a ser insertado en otros objetos | clases.
public interface BookRepository extends JpaRepository<Book, Long> {
}
