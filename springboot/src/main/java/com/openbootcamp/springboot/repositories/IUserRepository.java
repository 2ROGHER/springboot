package com.openbootcamp.springboot.repositories;

import com.openbootcamp.springboot.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<User, Long> {
}
