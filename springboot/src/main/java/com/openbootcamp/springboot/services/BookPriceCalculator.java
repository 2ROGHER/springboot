package com.openbootcamp.springboot.services;

import com.openbootcamp.springboot.models.Book;

/*
* Si queremos que este servicio pueda funcionar en otros componentes de nuestra app, tenemos que definir que ese es un @Bean
* para poder inyectar en la clase donde se quiera hacer uso.
* */
public class BookPriceCalculator {
    public double calculatePrice(Book book) {
        double price = book.getPrice();

        if(book.getPages() > 300) {
            price += 5;
        }

        // Coste de invio del book
        price += 2.99;
        return price;
    }
}
