package com.openbootcamp.springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

/*
* Configuration Swagger para la generacion de documentation de la API REST
* */
@Configuration
public class SwaggerConfig {
    // Esta clase nos permite configurar las anotaciones de Swagger
    @Bean
    public Docket api() {
        // Tenemos que inicializar un objeto de tipo Docket para poder usar las anotaciones.

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiDetails()) // terminos de uso y la lincencia entre otros mas.
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
    public ApiInfo apiDetails() {
        return new ApiInfo("springboot API REST books",
                "First description for api restful aplication",
                "932.3",
                "MIT",
                new Contact("hola", "htsfs;", "mit"),
                "mit",
                "fjsldf",
                Collections.emptyList());
    }
}
