package com.openbootcamp.springboot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfiguration {

    // ======================= PARTE DE JWT =====================
    @Autowired
    private UserDetailsServiceImpl userDetailServiceImpl;

    @Autowired
    private JwtAuthEntryPoint unauthorizedHandler;


    // ================= CREACION DE BEANS =============
    @Bean
    public JwtRequestFilter authenticationJwtTokenFilter() {
        return new JwtRequestFilter();
    }

    @Bean
    @Override
    public AuthenticationManager autehenticationManagerBean() throws Exception {
        return superautehenticationManagerBean();
    }



    // Tambien podemos agre agar un Firewall para habilitar si se permite agregar ciertos caracteres extranios en la ruta
    // Evitar el SQL Inyección
    @Bean
    public HttpFirewall looseHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        // podemos restringir la inyeccion de ciertas caracters
        firewall.setAllowBackSlash(true); // no permit
        firewall.setAllowSemicolon(true); // by default is false
        return firewall;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/hola") // esto nos permite acceer a la ruta "/hola", podemos establecer que la ruta "/hola**" sea accesible para que las rutas después de "/hola" sea mo]ificables.
                .antMatchers("/hola").hasRole("ADMIN")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .and()
                .httpBasic();

    }

    // habilitar un usuario en la memoria
    @Override
    protected void configure(AuthenticationManager auth) throws  Exception {
        // para la base de datos tenemos que codificar los passwords y el nombre del usuario.
        auth.inMemoryAuthentication()
                .passwordEncode(new BCryptPasswordEncoder())
                .withUser("user")
                .password(passwordEncoder().encode("password"))
                .roles("USER")
                .and()
                .withUser("admin")
                .password("password")
                .roles("USER", "ADMIN");
    }

    // Expose the UserDetailsService as Bean
    @Bean
    @Override
    public UserDetailsService userDetailsServiceBean() {
        return super.userDetailsServiceBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder () {
        return new BCryptPasswordEncoder();
    }

    // ================ CONFIGURACION GLOBAL PARA CORS =================

    // ============== OVERRIDE SOBRESCRIBIR LA FUNCIONALIDAD SECURITY POR DEFECTO =================================

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService((userDetailServiceImpl)).passwordEncoder(passwordEncoder());
    }




}
