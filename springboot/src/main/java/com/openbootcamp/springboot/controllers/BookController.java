package com.openbootcamp.springboot.controllers;

import com.openbootcamp.springboot.models.Book;
import com.openbootcamp.springboot.repositories.BookRepository;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Optional;

/*
* Un controller específico se tiene que anotar con @Controller annotation para decirle a Spring
* que la function se encarga de controlar para las rutas
* @Controller -> Se encarga de Controlar las vistas o templates de HTML.
* */
@RestController
public class BookController {
    @Value("${varexample")
    private String message;

    private final Logger log = LoggerFactory.getLogger(BookController.class);
    /*Hay tres formas de agregar esta dependecias o agregacin de dependecinas:
     * 1. Mediante atributos
     * 2. Mediante Constructores
     * 3. Mediante setter
     * */
    private BookRepository bookRepository;

    // Constructor para la inyeccion de dependecias.
    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @GetMapping("/index")
    public String index() {
        return "<h1>This is the heading of the index HTML page</h1>";
    }

    @GetMapping("/")
    public String HelloWorld() {
        return "Hello, World!";
    }

    /*
     * Funcion que devuelve una vista o un, témplate HTML como VISTA.
     *     * */
    @GetMapping("/view")
    public String view() {
        return """
                <h1>Hello, World!</h1>
                <p>Mi name is Roger Mestanza</p>
                <button>visit my webpage</button>
                """;
    }

    /*
     * Una API rest es un application de communication o de intercambio de de datos mediante JSON
     * Para ello tenemos que implementar un controlador para la implementacion de esta respuesta.
     * Todo endpoint tiene que seguir la siguiente nomenclatura. "http://localhost:8000/api/v1/books"
     * */
    @GetMapping("/api/books")
    public List<Book> findBooks() {
        return this.bookRepository.findAll(); // devolvemos toda la lista de los books.
    }

    /*
    * Ruta para el detalle para un solo libro
    * http://localhost:8081/api/v1/books/1
    *  */
    @GetMapping("/api/books/{id}")
    public Book findBookByID(@PathVariable("id") Long id) {
        Optional<Book> bookOption = this.bookRepository.findById(id);
        // Option 1.
        if (bookOption.isPresent()) {
            return bookOption.get();
        } else {
            return null;
        }

        // Option 2.
        // return bookOption.orElse();
    }

    // Podemos retornar una repuesta con HTTPResponse
    /*
    * Ruta para el detalle para un solo libro mediante respuestas tipo HTTPResponse.
    * http://localhost:8081/api/v1/books/1
    *
    * */
    @GetMapping("/api/books/{id}")
    public ResponseEntity<Book> HttpFindById(@PathVariable("id") Long id) {
        Optional<Book> bookOption = this.bookRepository.findById(id);
        // Option 1.
        if (bookOption.isPresent()) {
            return ResponseEntity.ok(bookOption.get());
        } else {
            // Other option.
            //return bookOption.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
            return ResponseEntity.notFound().build();
        }
    }

    // Variables de entorno
    @Value("${app.message}")// esta es la forma de como podemos acceder a las variables de entorno del sistema.
    @PostMapping("/api/books")
    public Book createBook(@RequestBody Book book, @RequestHeader HttpHeaders headers) { // Esto es similar a los headers, body, query que hay en node.js en los controllers.
        System.out.println(headers.get("User-Agent")); // Nos permite verificar el tipo de version y mas informacion acerca del equipo que esta realizando la peticon.
        return this.bookRepository.save(book); // Devuelve un objeto tipo book con una clave primaria.
    }

    /*
    * Metodo controller para actualizar un libro de una base de datos.\
    * http://localhost:8081/api/v1/books -> Metodo update para actualizar un Book objeto de la base de datos.
    * */

    @GetMapping("/api/books")
    public Book updateBook(@RequestBody Book book) {
        // planteamos las cabeceras para guardar los datos.
        Long id = book.getId(); // obtenemos el ID del objeto book.
        log.warn("Este es un framework de log que se usa para mostrar mensajes en la consola");
        // En estas líneas se pueden agregar más logica para controlar mejor la ruta.
        log.warn("Trying to create a new book in db, operation failed!");

        if(this.bookRepository.existsById(id)) {// esto nos permite verificar si existe el libro en la base de datos "SI o NO". De forma optima.
            System.out.println("");
        } else {
            log.error("Trying to update an unexciting book in the database");
        }

        this.bookRepository.findById(id); // si hay resultado en la base de datos con ID entonces recien los actualizamos.
        return bookRepository.save(book); // mismo metodo para guardar o actualizar el book en la base de datos.
    }

    /*
    * Metodo http para eliminar un libro de la base de datos.
    * http://localhost:8081/api/v1/books/{id}
    * Nota: Se debe devolver una respuesta HTTPResponse también.
    * */
    @ApiIgnore // Podemos ignorar este metodo para que no aparezca en la documentacion de la api Swagger dentro del navegador.
    @DeleteMapping("/api/books/{id}")
    public Optional<Book> deleteBook(@ApiParam("Clave primaria de tipo Long") @PathVariable Long id) {
        Optional<Book> book  = this.bookRepository.findById(id); // para poder devoler el resultado del delete
        this.bookRepository.deleteById(id); // encontramos y eliminamos de la base de datos
        return book; // retornamos el book de la base de datos.
    }
}
