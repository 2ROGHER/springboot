package com.openbootcamp.springboot;

import com.openbootcamp.springboot.models.Book;
import com.openbootcamp.springboot.models.User;
import com.openbootcamp.springboot.repositories.BookRepository;
import com.openbootcamp.springboot.initials.Coche;
import com.openbootcamp.springboot.initials.CocheRepository;
import com.openbootcamp.springboot.repositories.IUserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;

@SpringBootApplication
public class SpringbootApplication {

	@Bean
	public static PasswordEncoder crypt() {
		return new BCryptPasswordEncoder();
	}

	public static void main(String[] args) {
		// tenemos que extraer los Beans.
		ApplicationContext context  = SpringApplication.run(SpringbootApplication.class, args);
		CocheRepository repository = context.getBean(CocheRepository.class);
		IUserRepository userRepository = context.getBean(IUserRepository.class);

		// Obtenemos el metodo para poder encriptar  los datos.
		PasswordEncoder encoder = context.getBean(PasswordEncoder.class);


		// Crear y almacenar un coche en la base de datos.
		Coche toyota = new Coche(492423958, "Toyota", "fw392", 2032);
		repository.save(toyota);

		// Recuperar un coche por ID
		repository.findById(toyota.getId());

		// This is other
		BookRepository bookRepository = context.getBean(BookRepository.class);

		// Create new Book entity to store in DB.
		Book book  = new Book(null, "Homo Deus", "Cookies", 3423, 23.23, LocalDate.of(2018, 12, 12), true);

		bookRepository.save(book);

		bookRepository.findAll().size(); // find all books and show length.

		// Crear un objeto de tipo Usuario directamente aqui, pero recordar que este tarea lo hacemos en el Controller.
		User user1 = new User(null,"user1", "user1@user1.com", encoder.encode("user1passwordl"), encoder.encode("admin") );
		userRepository.save(user1);
		userRepository.save(new User(null,"user1", "user1@user1.com",encoder.encode("user2password"), "user2"));
		userRepository.save(new User(null,"user1", "user1@user1.com", crypt().encode("user3password"), "user2"));

	}

}
