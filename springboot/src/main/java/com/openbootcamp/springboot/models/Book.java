package com.openbootcamp.springboot.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
@Table(name="books") // Con esta annotation le decimos a Spring que cree una tabla con el nombre "books" en la DB.
@ApiModel("Clase libro para representar una entidad en la base de datos")
public class Book {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)// Con esto le decimos que nos cree relaciones
    private long id;
    private String title;
    private String author;
    private Integer pages;
    @ApiModelProperty("Precio en dolares, con dos decimales en la base de datos")
    private Double price;
    private LocalDate pubDate;
    private Boolean online;


    public Book(long id, String title, String author, Integer pages, Double price, LocalDate pubDate, Boolean online) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.pages = pages;
        this.price = price;
        this.pubDate = pubDate;
        this.online = online;
    }

    public Book(Long id, String title, String author, int pages, double price, LocalDate pubDate, boolean online) {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalDate getPubDate() {
        return pubDate;
    }

    public void setPubDate(LocalDate pubDate) {
        this.pubDate = pubDate;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
