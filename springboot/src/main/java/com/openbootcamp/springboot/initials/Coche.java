package com.openbootcamp.springboot.initials;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

/*
* Esta es la estructura general de un modelo que hay en una base de datos.
* */
@Entity

public class Coche {
    // atributos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // esto nos dice como es que se va a generar el id.Cuando le decimo que es `IDENTITY` se genera automaticamente por Spring
    private long id;
    private String manufacturer;
    private String model;
    private Integer year;

    // constructores
    public Coche(long id, String manufacturer, String model, Integer year) {
        this.id = id;
        this.manufacturer = manufacturer;
        this.model = model;
        this.year = year;
    }

    public Coche() {}
    // getters & setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    // to string
    @Override
    public String toString() {
        return "All data here";

    }
}
