package com.openbootcamp.springboot.initials;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
* Esta interfaz nos permite crear guardar el modelo cocheen la DB.
* Tenemos que agregar la anotacion de @Repository para decirle que es un repositorio.
* Ademas tenemos que decirle que es un @Bean, en donde spring es el encargado de crear este objeto Coche.
* JpaRepository, nos permite implementar los metodos CRUD de manera facil sin tener que preocuparnos por la implementacion.
*
*
*
* */
@Repository
public interface CocheRepository extends JpaRepository<Coche, Long> {

}
