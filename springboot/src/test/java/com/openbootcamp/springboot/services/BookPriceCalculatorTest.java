package com.openbootcamp.springboot.services;

import com.openbootcamp.springboot.models.Book;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*; // test de aserciones, que nos permiten verificar si funciona o no.

class BookPriceCalculatorTest {

    @Test // esta anotacion nos permite ejecutar el test, es importante colocar esta anotacion.
    void calculatePrice() {
        BookPriceCalculator calculator = new BookPriceCalculator();
        Book book  = new Book(null, "Homo Deus", "Cookies", 3423, 23.23, LocalDate.of(2018, 12, 12), true);
        // se ejecuta el comportamiento a testear
        double price = calculator.calculatePrice(book);

        System.out.println(price);

        // Comprobaciones || Aserciones
        assertTrue(price > 0);
        assertEquals(56.9234, price);
    }
}