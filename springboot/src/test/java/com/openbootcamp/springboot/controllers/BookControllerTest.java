package com.openbootcamp.springboot.controllers;

import com.openbootcamp.springboot.models.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;

import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT) // tenemos que decirle a SpringBoot que nos genere un test de tipo controlador con HTTP
class BookControllerTest {
    private TestRestTemplate testRestTemplate;
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @LocalServerPort
    private int port = 8081;
    // Con esto le decimo que tenemos que crear alguno código que necesitamos que se ejecute PRIMERO o antes de cada metodo.

    @Test
    void contextLoads() {
        System.getenv().forEach((key, value) -> System.out.println("key: " + key + " value: " + value));
    }

    @BeforeEach
    void setUp() {
        restTemplateBuilder =  restTemplateBuilder.rootUri("http://localhost:" + port + "/api/v1/books");
        testRestTemplate = new TestRestTemplate(restTemplateBuilder); // esto nos permite construir el tipo de puerto para nuestras pruebas.
    }


    @Test
    void helloWorld() {
        ResponseEntity<String> response = testRestTemplate.getForEntity("/hello", String.class); // en la ruta "/hello" nos permite el retorno como una respuesta en String.
        assertEquals(HttpStatus.OK, response.getStatusCode()); // esto os permite verificar que el codigo de estado es "200"
        assertEquals("Hola mundo aqui vamos con los tests", response.getBody()); // con esto verificamos que el mensaje que retorna sea el mismo que en el test.
    }

    @Test
    void findAll() {
         ResponseEntity<Book[]> response = testRestTemplate.getForEntity("/api/books", Book[].class);
         assertEquals(HttpStatus.OK, response.getStatusCode());
         assertEquals(200, response.getStatusCodeValue());

        List<Book> books = Arrays.asList(response.getBody());
        assertTrue(books.size() > 2);
    }

    @Test
    void findBookByID() {
        ResponseEntity<Book> response = testRestTemplate.getForEntity("/api/books/1", Book.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void createBook() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        String json = """
                {
                    "title": "title 1",
                    "author" : "author 1",
                    "pages" :  10234,
                    "price": 23.42,
                    "pubDate" : "2934-21-29",
                    "online" : true,
                }
                """;
        HttpEntity<String> request = new HttpEntity<>(json, headers);

        ResponseEntity<Book> response = testRestTemplate.exchange("/api/books/", HttpMethod.POST, request, Book.class);

        Book result = response.getBody();
        assertEquals(1L, result.getId());
        assertEquals("Libro creado desde el test", Book.class);


    }

    @Test
    void deleteBook() {
    }
}