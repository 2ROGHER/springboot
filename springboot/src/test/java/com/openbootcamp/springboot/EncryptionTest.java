package com.openbootcamp.springboot;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EncryptionTest {

    /*
    * Trata de un algoritmo que genera su propio "salt" de 16 bits.
    * Este "salt" es una palabra generada aleatoriamente para generar el proceso de criptar
    * Solo el usuario que cuente con el "salt" puede decifrar el contenido cifrado
    * El resultado de cifrar con bcrypt sera un string de 60 caracteres.
    *
    * $a version
    * $10 fuerza or complexity computational - modificable (valor que va de 4 - 31, por defecto vale 10)
    * Con esto sacrificamos la rapides de la validacion del hash, pero ganamos mas security.
    * Los 22 siguientes caracteres so el "salt" generado.
    *
    *
    * */
    @Test
    void bcryptTest() {
        // Este algoritmo se puede customizar para tener mayor proteccion.
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword  = passwordEncoder.encode("admin");
        System.out.println(encodedPassword); // print the encoded password

        // como hacer un "matching" con el password encoded
        passwordEncoder.matches("admin", encodedPassword);
        // failed case
        passwordEncoder.matches("adminfailed", encodedPassword); // failed or false.




    }

    @Test
    void bcryptCheckMultiplePasswords () {
        ArrayList<String> pwdList = new ArrayList<>();

        for(int i = 0; i <= 10; i++) {
            String pwd = new BCryptPasswordEncoder().encode("admin"); // veremos que esto nos genera la codificacion del mism password pero con distitntos "salts"
            pwdList.add(pwd);
        }

        for(String pwd : pwdList) {
            System.out.println("pwd: "  + pwd);
        }
    }


    @Test
    void argon() {
        Argon2PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();
        for(int i = 0; i <= 10; i++) {
            System.out.println(passwordEncoder.encode("admin")); // Podemos ver el codigo encriptado de varias formas.
        }
    }

    @Test
    void scrypt () {
        SCryptPasswordEncoder passwordEncoder = new SCryptPasswordEncoder();
        for(int i = 0; i <= 10; i++) {
            System.out.println(passwordEncoder.encode("admin")); // Podemos ver el codigo encriptado de varias formas.
        }

    }

    @Test
    void springPasswordEncoder() {
        String idForEncode = "bcrypt";
        Map<String, PasswordEncoder> encoders = new HashMap<String, PasswordEncoder>();
        encoders.put("bcrypt", new BCryptPasswordEncoder());
        encoders.put("pbkdf2", new Pbkdf2PasswordEncoder());
        encoders.put("argon2", new Argon2PasswordEncoder());
        encoders.put("scrypt", new SCryptPasswordEncoder());
        encoders.put("sa256", new StandardPasswordEncoder()); // deprecrated;

        PasswordEncoder passwordEncoder = new DelegatingPasswordEncoder("bcrypt", encoders);

        String encodedPassword = passwordEncoder.encode("admin");

    }
}
