## SpringData JPA
Este es importante para la implementacion de las conexiones con bases de datos.

### Importante
Demos instalar las dependencias de bases de datos para poder trabajar con la
persistencia de datos en una base de datos.

1. Spring Data JPA
2. Spring Data H2 (Driver para la persistencia de datos) || este es un driver se puede cambiar por cualquier base de datos.

Notas importantes ⚠️⚠️

- Todo modelo debe tener un `id` unico que lo identifique univocamente de entre un conjunto de elementos.
- Todo `id` no debe tener un campo vacio o nulo, dado que este no esta permitido para las bases de datos.
- Tenemos que indicarle que un `id` de una tabla es jsutamente una `id`. Eso se logra con la anotacion de `@id`. Esta anotacion de dice  a Spring que un atributo es un id.
- 
- Para que un modelo pueda ser almacenado en una base de datos, `debemos convertirlo en una ENTIDAD`.
- Para ello usamos la anotacion de `@Entity` para poder decirle a Spring que un modelo en concreto es una `Entitidad`.

- Ahora para poder guardar un modelo, tenemos que crear una `interfaz` para poder guardarlo en la base de datos con la ayuda de Spring.
- Para hacer esto primero se crea un `repositorio`.

- Ahora para poder tener utilizar de este repositorio podemos hacer uso en un `Controllador`. Pero para nuestro ejemplo lo usaremos direactmente en nuestro main.

- Las dependencias `starteds` son dependencias que dependen de otras dependencias. Esa es la strategia de las <dependecies>.

- Para las aplicaciones de `microservices` lo usual es cambiar los puertos de comunicacion para tener mas de una aplicacion en ejecucion y `listen`.

- Los `starterds` son dependencias que traen a su vez, o implementan otras dependecias por detras.
## Spring
Es una herramienta que nos permite crear proyectos poderosos de API-RESTFUL o proyectos WEB, poderosos de manera rapida y sencilla.

## Spring data 
Nos permite conectarnos con base de datos o metodos de persistencia de datos.

## Spring Boot
Este es un started rapido que nos permite crear un proyecto rapido, sin tener muchas configuarciones complejas y tediosas de servidore y arcihvos de por medio.

## Extensiones basicas para comenzar
1. Spring Web
2. Spring Dev Tools
3. Spring JPA
4. H2(Driver para la persistencia de datos local en archivos)

### Estructura para trabajar con archivos en Springboot
#### Entidad Book e.g
1. Entidad
2. BookRepository (permite almacenar los datos en la DB).
3. BookController(Nos permite controllar la comunicacion entre la entidad y la vista).
4. BookService (Aqui se realizan las validaciones o las implementaciones necesarias para poder validar los datos al 100% con ciertos lineamientos)
5. BookView(El template o la vista HTML que el usuario va a poder observar).
6. Para poder agregar un Logger a nuestra aplicacion tenemos que importar la clase interfaz "LoggerInterface".
## Servicios REST API.
Es un tipo de application que consiste en acoplar totalmente el back-end con el front-end.

### Microservicios
Consiste en modulirizar una aplicacion general en modulos mucho más pequenios, funcionalies y escalables a largo plazao.

Esta `arquitectura` nos permite implementar aplicaciones muy pequenias, pero que trabajan juntos *modularmente*.

Estas evitan la arquitectura `monoloitica`, en donde la aplicacion se modularaiza en applicaciones mas pequenias, para un tarea en concreto y en especifico, pero que 
funcionan perfectamente juntas y `comunicadas`.

![img.png](img.png)

La comunicacion entre los servicios generalmente se realizan mediante JSON.

### Operaciones CRUD en Springboot

### Springboot Swagger anotations
Para poder usar Swagger con nuestro proyecto de spring tenemos que instalar la dependencia de `SpringFox Boot Starter`.

```xml
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-boot-starter -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-boot-starter</artifactId>
    <version>3.0.0</version>
</dependency>

```

Por cada metodo controller que se implemente se genera una documentacion para el controlador creado. Esta es la ventaja del Swagger
que nos permite crear documentacion automaticamente.

Es una herramienta muy potente y alternativa rapida  que `PostmanApplication`.

Anotaciones importantes en Swagger

- `@ApiOperation`, toda anotacion se crea mediante `@Api...` y el metodo escodigo para indicar la anotacion correspondiente.
- La ruta para ver la docuemntacion en el navegador es: `http://localhost:8081/api/v1/swagger-ui/#/`, esta es la ruta donde podemos encontrar la pagina con los UIs de controlador.
- `@ApiModel`: Anotacin para indicarle a Swagger que es un modelo de datos.
- `@ApiModelProperty`: Esto nos permite indicar una propiedad del modelo.

### Testing en Spring Boot
- Para los tests, por defecto se usa  `JUnit Tests`.

#### Tests Unitarios
Estos tests son los encargados de testear las funciones unitarios o por `componentes` especificas del proyecto.

La intention de los `tests` es poder probar el codigo con datos ficticios y poder mejorar los errores del codigo.

Los tests nos permiten depurar los errores de codificacion del codigo y controlar posibles errores, crear codigos mas solidos y robustos para la produccion.

⚠️⚠️⚠️Importante: 

Los tests nos permite agregar la `covertura` de las pruebas, esto metodonos permite controlar el test, si queremos que 
se ejecute a nivel global o en componentes específicas y pequenias.

La finalidad de los `tests` es también tener una maxima convertura del proyecto.


Cuando se ejecute la application con los test, esto tardara un poco más dado con los test implementados para poder correr la aplicacion por completo.

## Gestion de ciclo de vida de una Aplicacion (Deploy de application java)
1. Clean (Limpiar la carpeta target)
2. Validate ()
3. Compile
4. Test
5. Package (Nos permite empaquetar para poder compilar la aplicacion)
6. Install (Instalar el mismo artefacto instalado, se le llama artefacto a la aplicacion ya compilado para poder compartir o que se ejecuta en otros entornos).
7. Site (Generar páginas de testing y covertura de la aplicacion)

### Entorno de production (Deploy Springboot application)
Para poder ejecutar la application tenemos que compilar y ejecutar la aplicacion en otros entonrnos o maquinas como servidores.
1. Pasos de para la compilacon de esta aplicacion.

```bash
$ # 1. Dentro de la pestania Maven tenemos que ejecutar el comando "package" para poder empaquetar la aplicacion para poder contruir la app.
$ # 2. Ver en la carpeta "target" teenmos que tener un archivo .jar que es como un .zip de la aplicacion.
$ # 3.Navegar hasta la carpeta "target" y ejecutar el siguiente comando
$ cd ./target
$ # 4. Una vez aqui tenemos que el siguiente comando para ejecutarlo ennuestro servidor local.
$ java -jar [jar_name_file.jar]
$ # Co el comando anterior lo que hemos echo es ejecutar nuestra application "spring" desde nuestro terminal. Ahora podemos ver que nuestra app, se puede ejecutar en cualquier maquina que tenga instalado Java.


```

### Configuracion de entornos de desarrollo
Dentro de una aplicacion se pueden tener diferentes entornos de desarrollo que se pueden implementar.
1. Entorno de desarrollo.
2. Entorno de produccion.

Cada uno de los entornos puede tener susu propios archivos de configuration y dependiendo de que entorno se usa podemos cargar uno u el otro.

⚠️⚠️
Se tienen que crear perfiles tanto para `dev` y otro para `test` con una propiedad nueva que carguemo en un controlador.


### Desplegar la aplicacion en la Nube
Para poder desplegar la aplicacion tenemos muchas alternativas, los cuales son proveedores que nos pemiten desplegar nuestra aplicacion en la nuve.

`Heroku`: Esta es solo una alternativa que nos permite desplegar la aplicacion mediante este servicio en linea.

Esta es una herramienta gratuita, pero para ciertos *features* se puede escoger la version de paga.

Para hacer esto tenemos que crear un archivo `system.properties` en la raiz del proyecto para poder especificar el tipo de Java que va a usar para poder desplegar la aplicacion.
```system.properties
java.runtime.version = 17
```
Para poder subir el proyecto a la nube, primeros tenemos que enviar a un el proyect aun repositorio remoto, de tal manera que mediante comandos desplegar en el servidor remoto.

Una vez subido el proyecto a un repositorio de `Github` tenemos que indicar a *Heroku* la `URL` del repositorio para que se pueda cojnectar y para qué puede desplegarlo en la nube.


![img_1.png](img_1.png)

Una vez guardado los cambios tenemos la posibilidad de activar la opcion `Desplegar automiticamente`, esto nos permite desplegar la aplicacion en cuando se ha un `commit` en el proyecto que se esta desplegando

Nadie debe hacer un `commit` en la rama de la produccion si no son los cambios que se quiere desplegar en la aplicacion.
Si se desea enviar cambios en produccion debe ser lo que se quiere ver en al en la version final de proyecto.

Tener en cuenta la `rama` de git en la que se está trabajando.

⚠️⚠️
Si tenemos en mente realizar muchos cambios en la aplicacion, pero no queremos que al momento de realizar un `commit` se muestren los cambios en el servido remoto como `Heroku`,
podemos crear una rama como `developer` para poder realizar los cambios y una vez que se han pasado las pruebas y estamos seguro que todo funciona
bien podemos agregarlo `merge` con la rama `master` de la aplicacion para se Heroku lo pueda actualizar.


#### Servidores en la nube para deploy
1. Netlifly (Frontend)
2. Heroku (Backend)
3. Versel (Frontend)

## Agregando Security a la aplicación en Springboot (Seguirdad)
Para poder agregar seguridad a la aplicacion tenemos que seguir los siguientes pasos.
1. Agregar las dependencias de `Seguridad` para la aplicación.
    - Spring Security -> Esta es la dependencia que se usa para agregar al seguridad en la aplicación de Springboot.
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>

```
Tener en cuenta que agregar la aplicacion es una forma de poder agregar seguridad a las rutas para que solo 
usuarios autorizados puedan ingresar o hacer uso de ciertas rutas.

Al monento de usar la aplicacion con `securidad` lo que nos devuelve es un `password` que nos permite trabajar con la seguridad de las rutas.

Si ingresemos a la rutas `http://localhost:8081/` Springboot nos devuelve un vista de *Login* para poder loggearnos en la aplicacion.
Tenemos que tener en cuenta que para iniciar sesoin tenemos que usar el password que devuelve y el `user` para iniciar sesion

Esto proceso nos permite crear una `session` donde las rutas estan en dicha session protegidos para cada usuario que se loggee en `registrado` en al *session*.

⚠️⚠️
Ahora para poder realizar peticiones a las rutas tenemos que agregar `authentication` en los headers de la requests.

Tenemos que usar el `Basic Authentication` dentro de la aplicacion de `postman` con el token para poder realizar las peticiones.

Todos estos pasos nos permiten crear una `session` y poder trabajar con las rutas dentro de la aplicacion.

📝📝📝
Para restringir que una determinada ruta que no sea accesible por el usuario podemos hacer lo siguiente:

1. Tenemos que crear una clase dentro de la carpeta de las configuraciones.
2. Con esto permitimos que ciertas rutas sean públicas o `privadas` solo para rutas y usuarios en concreto.


### Sistemas de cifrado para la encriptación de datos.
1. Para este ejemplo usaremos la base de datos `PostgreSQL` y `SpringJPA` , para ello tenemos que agregar las dependecias en diractamente desde 
las dependencias del `spring.starter.io`

```xml
<dependencies>

   <dependency>
      <groupId>org.postgresql</groupId>
      <artifactId>postgresql</artifactId>
      <scope>runtime</scope>
   </dependency>
   <dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
</dependencies>
```

#### Cifrado de aplicaciones con Springboot
1. El cifrado de las aplicaciones en especial de los datos es importante para encriptar y cifrar datos `sencibles` de la aplicacion
2. Es un proceso que consiste en verificar la información de su representacion original (text plano)
3. Consisten convertir un archivo de texto plano a un archivo de texto cifrado, donde solo el usuario que tenga el `token` puede ver el contenido cifrado.
4. Esto nos permite agregar capas de seguridad en la aplicacion
5. Para el envio de datos, o para el `almacenamiento` de datos se deben `encriptar` para que se pueda persisiter de forma segura en la DB.
6. También la finalidad es almacenar la información *password* entre otros de forma segura mediante `funciones hash` para poder encriptar los datos


#### Historia del Cifrado
1. Almacenar passwords en texto plano.
2. Almacenar password cifradas con una funcion *hash*.
3. Almacenar passwords cifradas con una funcion *hash* + *salt* (es una capa de seguridad para evitar desencriptarlo)
4. Almacenar password cifradas con una funcion *adaptativa* (Factor de coste o trabajo computacional complejo para que tome mas tiempo)

#### Algoritmos en Spring Security
* BCrypt
* PBKDF2
* scrypt
* argon2

La finalidad de estas funciones es el mismo `encriptar` pero el modelo matematico que utiliza es diferente agregando complejidad y funciones de coste 
de trabajo diferente.



More info: [bcrypt-hash](https://cs.wellesley.edu)

### Authentication de Usuarios con JWT (Uso de JWT en SpringBoot)
Este es un tipo de estandar que nos permite, transmitir informacion entre dos partes: Navegador(cliente), Servidor.

### Funcinamiento de una Session
1. Cliente envia una peticino a un servidor (/api/login/)
2. Serividor válida el `username` y el `password`, si no son válidos devolvera una respuesta `401` no autorizado.
3. Servidor válida el `password` y el `username`, si son válidos, entonces se almacena el `username` en la *session* del navegador.
4. Se genera una `cookie` en el lado del cliente
5. En las proximas sesiones se comprueba que el ciente esta presente en al session.
6. Sino esta en al session se le dice al cliente loggearse nuevamente.
7. La informacin de la session  se almacena en el servidor, lo cual consume recursos.
### Funcionamiento de JWT
1. Cliente envia una peticion a un sevidor(/api/login/), la information del login mediante un formulario viaja `cifrado`. Metodo usado para esto POST.
2. Serividor válida el `username` y el `password`, si no son válidos devolvera una respuesta `401` no autorizado.
3. Servidor válida el `password` y el `username`, si son válidos, genera un `token` JWT utilizando un secret key que se puede configurar en las variables de entorno.
4. El servidor devuelve el `token JWT` generado al cliente (al cliente que hizo la peticion)
5. Cliente envia peticiones a los *endpoints* REST del servidor utilizando el token JWT en los `headers`.
6. Servidor válida el *toke JWT* en cada peticion y si es valido permite el acceso a los datos de la API.

#### Ventajas
* El token no se almacena en el cliente, sino se guarda en el navegador del cliente, de manera que consume menos 
recursos en el servidor, lo cual permite mejor escalabilidad.
* Dado que el token se almacena en el navegador, esto consume menos recursos del servidor, lo cual mejora y permite mejor escalabilidad de la aplicacin.
* En el lado del servidor hace mas facil dado que no consume muchos recursos de este.
#### Desventajas
* El token esta en el navegador, no podriamos invalidarlo antes de la fecha de expiracion asignada cuando se creyó
* Con esto lo que se puede hacer es dar una opcin de `loggout` para poder caducar la session.


### Estructura de JWT
* Tiene 3 partes separadas por (.) y codificadas en base 64 partes cada una.
* 1. Header: Estos son los metadatos.
```json
{
   "algoritmo": "HS512",
   "type": "JWT"
}
```
* 2. Paylod (informacion, datos del user, no sesible para el navegador)
```json

{
   "name": "Jonh Doe",
   "admin": true
}
```
* 3. Signature (Algoritmo aplicando encriptacion)

```
   HMACSHA256(base64UrlEncode(header) + "." + base64UrlEncode(payload), secret)

```
* El User-Agent (navegador) envia el token JWT a la API, en las cabeceras:
```
Authorization: Bearer [token]
```
Apartir de ahora para poder realizar las peticinoes `request` se tiene que usar este token para poder acceder a los recursos o los datos de la API.

#### Configuracion de Spring | Instalacion de dependecias en par agregar Securidad a Spring.
1. Crear un proyecto con Srping Boot con: 
   * Spring security
   * Spring web
   * Spring boot devtools
   * Spring Data JPA
   * PostgreSQL
   * Dependencia JWT (se aniade manualmente en JWT (sitio oficial))
   
```xml
<dependency>
   <groupId>io.jsonwebtoken</groupId>
   <artifactId>jjwt</artifactId>
   <version>0.9.1</version>
</dependency>
```
#### Estructura de carpetas en Sprigboot
1. models
2. dto (Estos son clases de objetos cuyo objetivo es `devolver` datos al FRONT-END. Es solo para enviar datos al cliente)
3. repositoy
4. service (son clases que situamos entre un `repositorio` y un `controlador`), este generalmente tiene una `interfaz` y si `implementacion` correspondiente.
5. security
   - config
   - jwt
   - payload
   - service
6. config
7. controllers
8. application.properties (permite agregar propiedades para conexin de bases de datos, variables de entorn entre otros).


## OAuth 2 con Spring 
Significa (Open Authoritation)

Es un framework de autorizacion  y `seguridad`

Es de codigo abierto y esta construido en base de estandares  IETF y licence de  Open Web Foundation.

Es un protocoljo de delegacion: 

Permite que alguien que controla un recurso (*google, github, etc*) o un aplicacion de software de terceros acceceder a ese recurso
en su propio nombre sin pasar por ellos.

Flujo de trabajo: 

1. Una aplicacion solicita authentication (descargamos una aplicacion de instagram este solicita logearse para realizr ciertas acciones)
2. Se realiza el login mediante Google, Github, Facebook, etc. Exiten mas proveederoes para realizar el registro  y el login
3. La aplicacion se comunica con Google u otro servicioo, donde utlia las credenciales de Google sin que la aplicacion  pueda verlas (aparece una ventana popup para poder iniciar session)
4. El servidor de Google pregunta al usuario si desea conceder los permisos
5. Google genera un token de acceso como respuesta a la peticion del servidor al usuario.

### Escenarios de uso de OAuth
1. Authentication HTTP en la uqe no se quiere usar el user y el password todo el tiempo para estos procesos.
2. Cuando tenemos multiples aplicaciones y no queremos estar iniciando session a cada moemento con nuestras credenciales (proceso tedioso).
3. Se puede aplicar en `microservicios`, en donde podemos tener un servidor central que pueda gestionar la autenticacion de los usuarios que cuentan con varias cuentas y contrasenas el cual en ciertos casos es tedioso y aburrido. 
4. Interaccion en las aplicaciones de terceros. Cuando se tenga que instalar las aplicaciones de terceros y estos permitan acceder a sus servicion con previa inisio de session

### Proveedores de protocolos de delegacion
Con la ayuda de OAuth los usuarios pueden autorizar a third part applications a acceder a sus datos  o ejecutar determinads operacines sin necesidad 
de proporcionar un usuario y contrasena.

Google, Facebook, Github, Gitlab, etc

### OAuth en Spring Security
Inicialmente habia un proyecto llamando Spring Security OAuth.

En 2018 se sobreescribe para hacerlo mas eficiente, con un codigo base mas sencillo.

Se depreca el antiguo OAuth y ahora esta integrado sobre le propio Spring Security para poder usarlo facil y seguro.

Este metodo es un paso más arriba de la `seguridad`. Es un proceso más avanzado para la seguridad de la aplicacion.



Incluye: 

* Client Support
* Resource Server https://github.com/spring-projects/spring-authorization-server/

### Necesitamos un paso más para hacer esto, pero: 
* Necesitamos un Authorization Server para realizar las autorizaciones, pero esto de momento esta en fase (Beta). 

### Flujos de Accion
* Authorization code 
* Implicit 
* Resource Owner password credentials
* Client Credentials
* Refresh token 

### OpenID Connect
* OAuth 2.0 -> autorization de por encima.
* HTTP connections

## Notas ⚠️⚠️⚠️
1. La mejor forma de agregar datos en la base de datos es en el módulo de `tests`.
2. 